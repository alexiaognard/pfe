* Improve documentation for `$LINTER_RULES_PATH`
* Default value for `$LINTER_RULES_PATH` is now `.linters`

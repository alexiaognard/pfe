* Give the priority to `package-lock.json` to fetch package dependencies
* The possibility to use `npm ci` instead of `npm install`
# Project information
site_name: R2Devops hub
site_description: 'Ready to use CI/CD Jobs for your Pipelines !'
site_author: 'Go2Scale <cto@go2scale.com>'
site_url: 'https://r2devops.io/'

# Copyright
copyright: '
Copyright &copy; 2021 Go2Scale

'
terms_of_use: '/terms-of-use'
privacy: '/privacy'

# Repository
repo_name: 'r2devops/hub'
repo_url: 'https://gitlab.com/r2devops/hub'
edit_uri: 'edit/latest/docs/'

# Navigation
nav:
  - Home: index.md
  - Get started:
    - Concept: concept.md
    - Use the hub: use-the-hub.md
    - Versioning: versioning.md
    - R2bulary: r2bulary.md
  - Contributing:
    - How to: how-to-contribute.md
    - Structure of a job: job-structure.md
    - Create or update a job: create-update-job.md
  - ...

# Customization
extra:

  # Landing page
  landing_page:
    title: 'With R2Devops, create your CI/CD pipeline in 3 clicks'
    description: 'Benefit from our collection of ready-to-use jobs to quickly create amazing pipelines for your projects. The setup will only take you 3 clicks: just select the jobs and enjoy your CI/CD !'

  social:
    - icon: 'fontawesome/brands/discord'
      link: 'https://discord.r2devops.io/?utm_medium=website&utm_source=r2devops&utm_campaign=footer'
    - icon: 'fontawesome/solid/globe'
      link: 'https://go2scale.com/'
    - icon: fontawesome/brands/twitter
      link: https://twitter.com/Go2Scale
    - icon: 'fontawesome/brands/gitlab'
      link: 'https://gitlab.com/r2devops/'
    - icon: 'fontawesome/brands/youtube'
      link: 'https://www.youtube.com/channel/UCVt39An0xR1PJaX3_Me_zkw'
    - icon: 'fontawesome/brands/linkedin'
      link: 'https://www.linkedin.com/company/go2scale/'


extra_css:
    - css/extra.css
    - css/custom_admonitions.css
    - css/juxtapose/juxtapose.css
    - css/splide/splide-core.min.css
    - css/splide/splide.min.css
    - css/splide/extra.css

extra_javascript:
    - js/juxtapose/juxtapose.min.js
    - js/juxtapose/juxtapose.loader.min.js
    - js/splide/splide.min.js
    - js/splide/splide.loader.js
    - js/extra.js

# Configuration
theme:
  name: 'material'
  custom_dir: 'overrides'
  # Color
  palette:
    scheme: 'go2scale'
  # Text
  language: 'en'
  direction: 'ltr'
  font:
    text: 'Roboto'
    code: 'Roboto mono'
  # Images
  favicon: 'images/go2scale_white.svg'
  logo: 'images/go2scale_white.svg'
  features:
    - navigation.tabs
    - navigation.instant

markdown_extensions:
  - abbr
  - attr_list
  - admonition # block-styled content (note, idea, warning, ...)
  - codehilite # highlite code syntax
  - footnotes # references in footnotes
  - toc:
      permalink: true # anchor on titles
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.tabbed
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - meta


# Plugins (https://squidfunk.github.io/mkdocs-material/plugins/minify-html/)
plugins:
  - search # search input
  - git-revision-date-localized # revision date at the end of pages
  - awesome-pages
  - minify:
      minify_html: true # minification of html

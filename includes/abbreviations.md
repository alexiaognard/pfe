*[Pipelines]:
Set of jobs launched in the same sequence. Pipeline can be run manually, after a commit or a merge into a branch. We consider the pipeline as succeeded if all jobs in the pipeline success.

*[Pipeline]:
Set of jobs launched in the same sequence. Pipeline can be run manually, after a commit or a merge into a branch. We consider the pipeline as succeeded if all jobs in the pipeline success.


*[Job]:
A job is a script hosted in R2Devops Hub that can be included in CI/CD pipeline to do a unitary work.


*[Jobs]:
A job is a script hosted in R2Devops Hub that can be included in CI/CD pipeline to do a unitary work.


*[CI]:
Continuous integration is a coding philosophy and set of practices that drive development teams to implement small changes and check in code to version control repositories frequently. This will ensure that your application meets your criteria of quality, security, performance, after each modification of your code.

*[CD]:
Continuous delivery starts where continuous integration ends. CD philosophy is to automate the delivery of applications to the selected infrastructure environments.

*[Hub]:
The R2Devops hub is a collaborative hub of CI & CD ready to use jobs which helps you to easily build powerful pipelines for your projects. Anyone can contribute and propose his own job and get feedback from the community.

---
title: How to contribute   
description: You want to contribute to R2Devops? You are in the right place! Find out easily where to go regarding the type of contribution you want to make, and help to develop this platform.
---

# How to

Welcome to the contributing guide of R2Devops hub 🥳 Glad to see you here! 😀

This page will guide you to the right place depending on the kind of contribution you want to do:

* **Understand the structure of a job** 👉 Go to [structure of a
  job](/job-structure/).
<!-- * **Suggest a feature, a new job or report a bug** 👉 [Create an
  issue](/create-issue/) -->
* **Create or update a public job in R2Devops hub repository** 👉 Go to [create or
  update a job](/create-update-job/).

  
!!! heart "Community"
    We love talking with our contributors and users! Join our
    [Discord community :fontawesome-brands-discord:](https://discord.r2devops.io/?utm_medium=website&utm_source=r2devops)
